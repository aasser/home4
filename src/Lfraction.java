import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 * @author Anneli Asser
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      System.out.println(valueOf(","));// TODO!!! Your debugging tests here
   }

  
   private long numerator, denominator;
   

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
	  if(b == 0){
	         throw new RuntimeException("Denominator is zero");
	      }
	  
	  this.numerator = a;
	  this.denominator = b;
	  
	  if (denominator == 0)
	         throw new ArithmeticException (" illegal denominator zero ");
	      if (denominator < 0) {
	         numerator = -numerator;
	         denominator = -denominator;
	      }
	      if (numerator == 0)
	         denominator = 1;
	      else {
	         long n = gcd (numerator, denominator);
	         numerator = numerator / n;
	         denominator = denominator / n;
	      }
      
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator; 
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.denominator; 
   }

   /** Conversion to string.
    * @return string representation of the fraction
    * snippet of code from ftp://www1.idc.ac.il/coursesFTP/cs/intro1/Exercises/Ex02/Fraction.java
    */
   @Override
   public String toString() {
	   String result;
	      if (numerator == 0)
	         result = "0";
	      else
	         if (denominator == 1)
	            result = numerator + ""; // return it as a string
	         else
	            result = numerator + "/" + denominator;
	      return result; 
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    * snippet of code from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   @Override
   public boolean equals (Object m) {
	   return (this.compareTo ((Lfraction)m) == 0); 
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    * snippet of code from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   @Override
   public int hashCode() {
	   return (int) (Double.doubleToLongBits (toDouble())>>31);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    * snippet of code from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction plus (Lfraction m) {
	  if ((this.denominator == 0) || (m.denominator == 0))
	         throw new IllegalArgumentException("invalid denominator");
	  return new Lfraction (numerator*m.denominator + denominator*m.numerator,
		         denominator*m.denominator).reduce(); 
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    * snippet of code from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction times (Lfraction m) {
	   if ((this.denominator == 0) || (m.denominator == 0))
	         throw new IllegalArgumentException("invalid denominator");
	   return new Lfraction (numerator*m.numerator, denominator*m.denominator)
		         .reduce(); 
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    * snippet of code from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction inverse() {
	   if ((this.denominator == 0)) throw new IllegalArgumentException("invalid denominator");
	   return new Lfraction (denominator, numerator).reduce(); 
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    * snippet of code from http://gee.cs.oswego.edu/dl/classes/EDU/oswego/cs/dl/util/concurrent/misc/Fraction.java
    */
   public Lfraction opposite() {
		  long nm = getNumerator();
		  long dm = getDenominator();
		  return new Lfraction(-nm, dm);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    * snippet from ftp://www1.idc.ac.il/coursesFTP/cs/intro1/Exercises/Ex02/Fraction.java modified to subtract
    */
   public Lfraction minus (Lfraction m) {
	   if ((this.denominator == 0) || (m.denominator == 0))
	         throw new IllegalArgumentException("invalid denominator");
	  return new Lfraction (
            (numerator * m.getDenominator() - m.getNumerator() * denominator) ,
             denominator * m.getDenominator() ); 
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    *sippet of code from https://github.com/egaia/AlgorithmAndDataStructure/blob/master/home4/src/Lfraction.java
    */
   public Lfraction divideBy (Lfraction m) {
	   if ((this.denominator == 0) || (m.denominator == 0))
	         throw new IllegalArgumentException("invalid denominator");
	   return this.times(new Lfraction(m.denominator, m.numerator)); 
   }

   /** Comparison of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	   Lfraction b = (Lfraction)(m);
	    long an = getNumerator();
	    long ad = getDenominator();
	    long bn = b.getNumerator();
	    long bd = b.getDenominator();
	    long l = an*bd;
	    long r = bn*ad;
	    return (l < r)? -1 : ((l == r)? 0: 1);
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator); 
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    * http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public long integerPart() {
	   Lfraction tmp = reduce();
	      return tmp.getNumerator()/tmp.getDenominator();
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    * http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public Lfraction fractionPart() {
	   return new Lfraction (numerator - denominator*integerPart(), denominator)
		         .reduce();
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
	   return ((double)(getNumerator())) / ((double)(getDenominator())); 
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    * from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public static Lfraction toLfraction (double f, long d) {
	   if (d > 0) {
	         return new Lfraction((Math.round(f * d)), d);
	      }else{
	         throw  new RuntimeException("Illegal denominator");
	      }
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    * inspiration for code snippet from http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   public static Lfraction valueOf (String s) {
	      if (s.trim().length() == 0) {
			throw new RuntimeException("Empty string");
		}
	      StringTokenizer st = new StringTokenizer (s, "/<>[](),");
	      long numerator = 0;
	      long denominator = 1;
	      
	      try {
	      if (st.hasMoreTokens()) {
	    	  numerator = Long.parseLong (st.nextToken().trim());
	      } else {
	         throw new IllegalArgumentException ("not fraction " + s);
	      } } catch (Exception e) {
	    	  throw new IllegalArgumentException ("Murd ei tohi olla selline: " + s);
		}

	      
	      try {
	    	  if (st.hasMoreTokens()) {
		    	  denominator = Long.parseLong (st.nextToken());
		    	  
		      } else {
		         // throw new IllegalArgumentException ("not fraction " + s);
		    	  denominator = 1;
		    	 
		      }
		} catch (Exception e) {
			throw new IllegalArgumentException ("Murd ei tohi olla selline: " + s);
		}
	      
    
	      if (st.hasMoreTokens()) {
	         throw new IllegalArgumentException ("not fraction " + s);
	      }
	      return new Lfraction (numerator, denominator);
	      
	      
	   } 
   
   /** Greatest common divisor of two given integers.
    * @param a first integer
    * @param b second integer
    * @return GCD(a,b)
    * snippet of code taken from: http://aleph0.clarku.edu/~djoyce/cs101/Resources/Fraction.java
    */  
   public static long gcd(long numerator, long denominator) {
      long factor = denominator;
      while (denominator != 0) {
         factor = denominator;
         denominator = numerator % denominator;
         numerator = factor;
      }
      return numerator;
   }
 
   /** Reduce this fraction (and make denominator > 0).
    * @return reduced fraction
    * snippet of code taken from: http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    */
   private Lfraction reduce() {
	   Lfraction f = null;
      try {
         f = (Lfraction)clone();
      } catch (CloneNotSupportedException e) {};
      if (denominator == 0)
         throw new ArithmeticException (" illegal denominator zero ");
      if (denominator < 0) {
         f.numerator = -numerator;
         f.denominator = -denominator;
      }
      if (numerator == 0)
         f.denominator = 1;
      else {
         long n = gcd (numerator, denominator);
         f.numerator = numerator / n;
         f.denominator = denominator / n;
      }
      return f;
   }
}

